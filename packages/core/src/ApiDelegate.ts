export default interface ApiDelegate<AuthInfo> {
  getHeaders: (authInfo: AuthInfo | undefined) => any;

  getAuthInfo: () => Promise<AuthInfo | undefined>;
  updateAuthInfo: (authInfo: AuthInfo) => Promise<void>;
}
