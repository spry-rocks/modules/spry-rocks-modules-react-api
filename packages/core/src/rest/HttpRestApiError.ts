import {AxiosError} from 'axios';
import RestApiError from './RestApiError';

export default class HttpRestApiError extends RestApiError {
  constructor(
    public readonly message: string,
    public readonly status: number,
    public readonly statusText: string,
    public readonly data: any,
    public readonly cause: AxiosError,
  ) {
    super(message, cause);

    Object.setPrototypeOf(this, HttpRestApiError.prototype);
  }
}
