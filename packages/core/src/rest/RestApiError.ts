import ApiError from '../ApiError';
import {AxiosError} from 'axios';

export default class RestApiError extends ApiError {
  constructor(public readonly message: string, public readonly cause: AxiosError) {
    super(message, cause);

    Object.setPrototypeOf(this, RestApiError.prototype);
  }
}
