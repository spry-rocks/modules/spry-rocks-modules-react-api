import {AxiosRequestConfig} from 'axios';

export type DownloadFileConfig = {
  absolute?: boolean;
  method?: 'GET' | 'POST';
  directory?: string;
};
export type DownloadFileResponse = {path: string; name: string; directory: string};

export interface INetwork {
  request<T>(config: AxiosRequestConfig): Promise<T>;

  get<T>(url: string, config?: AxiosRequestConfig): Promise<T>;

  delete(url: string, config?: AxiosRequestConfig): Promise<void>;

  head(url: string, config?: AxiosRequestConfig): Promise<void>;

  post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;

  put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;

  patch<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;

  uploadFile<T>(url: string, file: string | File, name: string): Promise<T>;

  downloadFile(
    url: string,
    data?: any,
    config?: DownloadFileConfig,
  ): Promise<DownloadFileResponse>;
}
