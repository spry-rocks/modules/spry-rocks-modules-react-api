import {INetwork} from './INetwork';

export interface INetworkFactory {
  createNetwork(baseURL: string, headers: object, getHeaders: () => object): INetwork;
}
