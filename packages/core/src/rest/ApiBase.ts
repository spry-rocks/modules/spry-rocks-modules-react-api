import ApiConfiguration from './ApiConfiguration';
import ApiDelegate from '../ApiDelegate';
import IApiAuthHolder from '../IApiAuthHolder';
import {INetwork} from './INetwork';
import {INetworkFactory} from './INetworkFactory';

export default class ApiBase<AuthInfo> {
  private static readonly HEADERS = {};

  public readonly url: string | undefined;

  protected readonly network: INetwork;

  constructor(
    baseUrl: string,
    configuration: ApiConfiguration,
    delegate: ApiDelegate<AuthInfo>,
    tokenHolder: IApiAuthHolder<AuthInfo>,
    networkFactory: INetworkFactory,
  ) {
    this.url = `${baseUrl}${configuration.path}`;
    this.network = networkFactory.createNetwork(this.url, ApiBase.HEADERS, () =>
      delegate.getHeaders(tokenHolder.getAuthInfo()),
    );
  }
}
