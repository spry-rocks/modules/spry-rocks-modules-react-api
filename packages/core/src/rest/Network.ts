import axios, {
  AxiosError,
  AxiosInstance,
  AxiosPromise,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import * as AxiosLogger from 'axios-logger';
import HttpRestApiError from './HttpRestApiError';
import {INetwork, DownloadFileConfig} from './INetwork';
import {detectPlatform} from '../utils/PlatformUtils';
import RestApiError from './RestApiError';
import RNFetchBlob from 'rn-fetch-blob';
import {v4 as uuid} from 'uuid';

export class Network implements INetwork {
  private readonly api: AxiosInstance;

  constructor(
    private readonly baseURL: string,
    headers: object,
    private getHeaders: () => object,
  ) {
    this.api = axios.create({
      baseURL,
      headers,
    });
    this.api.interceptors.request.use((request) => {
      return {
        ...request,
        headers: {
          ...request.headers,
          ...getHeaders(),
        },
      };
    });
    this.api.interceptors.request.use(AxiosLogger.requestLogger, AxiosLogger.errorLogger);
    this.api.interceptors.response.use(
      AxiosLogger.responseLogger,
      AxiosLogger.errorLogger,
    );
  }

  // region methods
  request<T>(config: AxiosRequestConfig): Promise<T> {
    return Network.wrapApiCall(this.api.request(config));
  }

  get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return Network.wrapApiCall(this.api.get(url, config));
  }

  delete(url: string, config?: AxiosRequestConfig): Promise<void> {
    return Network.wrapApiCall(this.api.delete(url, config));
  }

  head(url: string, config?: AxiosRequestConfig): Promise<void> {
    return Network.wrapApiCall(this.api.head(url, config));
  }

  post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return Network.wrapApiCall(this.api.post(url, data, config));
  }

  put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return Network.wrapApiCall(this.api.put(url, data, config));
  }

  patch<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return Network.wrapApiCall(this.api.patch(url, data, config));
  }
  // endregion

  // region files
  public async uploadFile<T>(url: string, file: string | File, name: string) {
    const form = Network.createFormWithFile(file, name);
    return this.post<T>('files', form, {
      headers: {
        ...this.getHeaders(),
        'Content-Type': 'multipart/form-data',
        data: form,
      },
    });
  }

  public async downloadFile(url: string, data?: any, config?: DownloadFileConfig) {
    let finalUrl = url;
    if (!config?.absolute) {
      finalUrl = `${this.baseURL}/${url}`;
    }

    const {dirs} = RNFetchBlob.fs;

    const directory = config?.directory ?? dirs.DownloadDir;
    const name = uuid();
    const path = `${directory}/${name}`;

    const response = await RNFetchBlob.config({
      path,
    }).fetch(
      config?.method ?? 'GET',
      finalUrl,
      {
        ...this.getHeaders(),
        'Content-Type': 'application/json',
      },
      JSON.stringify(data),
    );

    const statusCode = response.info().status;
    if (statusCode !== 200) {
      throw new Error(`Bad status code ${statusCode}`);
    }

    return {path, name, directory};
  }

  private static createFormWithFile(file: string | File, name: string) {
    const platform = detectPlatform();
    switch (platform) {
      case 'web': {
        return this.createFormWithFileWeb(file, name);
      }
      case 'react-native': {
        return this.createFormWithFileRN(file, name);
      }
      default:
        throw new Error(`Not supported platform: ${platform}`);
    }
  }

  private static createFormWithFileWeb(uri: string | File, name: string) {
    const form = new FormData();
    const blob = new Blob([uri], {type: 'image/jpeg'});
    form.append('file', blob, name);
    return form;
  }

  private static createFormWithFileRN(uri: string | File, name: string) {
    const form = new FormData();
    const data = {
      uri,
      type: 'image/jpeg',
      name,
    };
    // @ts-ignore
    form.append('file', data);
    return form;
  }
  // endregion

  // region utils
  protected static wrapApiCall(call: AxiosPromise): Promise<any> {
    return call
      .then((response) => Promise.resolve(Network.handleResponse(response)))
      .catch((response) => Promise.reject(Network.handleError(response)));
  }

  private static handleError(error: AxiosError) {
    const {response} = error;
    if (!response) {
      return new RestApiError(error.message, error);
    }

    return new HttpRestApiError(
      error.message,
      response.status,
      response.statusText,
      response.data,
      error,
    );
  }

  private static handleResponse<T>(response: AxiosResponse<T>) {
    return response.data;
  }
  // endregion
}
