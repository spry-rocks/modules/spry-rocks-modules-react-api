import IApiTokenHolder from './IApiAuthHolder';

export default class ApiAuthHolder<AuthInfo> extends IApiTokenHolder<AuthInfo> {
  private authInfo: AuthInfo | undefined = undefined;

  setAuthInfo(authInfo: AuthInfo | undefined) {
    this.authInfo = authInfo;
  }

  getAuthInfo(): AuthInfo | undefined {
    return this.authInfo;
  }
}
