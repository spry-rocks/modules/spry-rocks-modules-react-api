import ApiConfiguration, {getApiConnectionOptions} from './ApiConfiguration';
import Queue from 'promise-queue';
import ApiDelegate from './ApiDelegate';
import ApiAuthHolder from './ApiAuthHolder';
import IApiBase from './IApiBase';
import {isNotAuthorizedError} from './ErrorUtils';

export default abstract class ApiBase<AuthInfo, Delegate extends ApiDelegate<AuthInfo>>
  implements IApiBase<AuthInfo> {
  private readonly refreshQueue = new Queue(1, Infinity);

  protected readonly baseUrl: string;

  protected readonly wsBaseUrl: string;

  public readonly authHolder = new ApiAuthHolder<AuthInfo>();

  constructor(
    protected readonly configuration: ApiConfiguration,
    protected readonly delegate: Delegate,
  ) {
    const {baseUrl, wsBaseUrl} = getApiConnectionOptions(configuration);
    this.baseUrl = baseUrl;
    this.wsBaseUrl = wsBaseUrl;
  }

  protected async wrapApiCall<TResponse>(
    call: () => Promise<TResponse>,
  ): Promise<TResponse> {
    try {
      return await call();
    } catch (e) {
      if (
        this.configuration.refreshTokensOnNotAuthorizedError &&
        isNotAuthorizedError(e)
      ) {
        await this.refreshQueue.add(() => this.refreshTokens());
        // eslint-disable-next-line no-return-await
        return await call();
      }
      throw e;
    }
  }

  private async refreshTokens() {
    const authInfo = await this.delegate.getAuthInfo();
    if (!authInfo) {
      throw new Error('Not authorized');
    }
    const authResponse = await this.refreshToken(authInfo);
    await this.delegate.updateAuthInfo(authResponse);
    this.authHolder.setAuthInfo(authResponse);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,class-methods-use-this
  refreshToken(authInfo: AuthInfo): Promise<AuthInfo> {
    throw new Error('refreshToken not implemented, you must override this method');
  }

  getAuthHolder() {
    return this.authHolder;
  }
}
