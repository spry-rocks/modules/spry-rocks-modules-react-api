export default abstract class IApiAuthHolder<AuthInfo> {
  abstract getAuthInfo(): AuthInfo | undefined;

  abstract setAuthInfo(authInfo: AuthInfo | undefined): void;
}
