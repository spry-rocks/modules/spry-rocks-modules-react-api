export default interface ApiConfiguration {
  path: string;
  connectWebsocketTransport?: boolean;
}
