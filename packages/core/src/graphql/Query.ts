export type Query<TVariables, TData> = {
  query: any;
  variables: TVariables;
};

export type QueryFunc<TData> = () => Query<undefined, TData>;

export type QueryWithVariablesFunc<TVariables, TData> = (
  variables: TVariables,
) => Query<TVariables, TData>;

export const createQuery = <TData>(query: any): QueryFunc<TData> => () => ({
  query,
  variables: undefined,
});

export const createQueryWithVariables = <TVariables, TData>(
  query: any,
): QueryWithVariablesFunc<TVariables, TData> => (variables: TVariables) => ({
  query,
  variables,
});

export const createMutationWithVariables = createQueryWithVariables;

export const createSubscription = createQuery;
export const createSubscriptionWithVariables = createQueryWithVariables;
