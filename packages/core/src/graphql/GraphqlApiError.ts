import ApiError from '../ApiError';
import {ApolloError} from 'apollo-client';

export default class GraphqlApiError extends ApiError {
  constructor(public readonly message: string, public readonly cause: ApolloError) {
    super(message, cause);

    Object.setPrototypeOf(this, GraphqlApiError.prototype);
  }
}
