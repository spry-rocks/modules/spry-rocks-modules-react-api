import ApolloClient, {ApolloError, OperationVariables} from 'apollo-client';
import {HttpLink, InMemoryCache, NormalizedCacheObject, split} from 'apollo-boost';
import {setContext} from 'apollo-link-context';
import {Query} from './Query';
import ApiConfiguration from './ApiConfiguration';
import ApiDelegate from '../ApiDelegate';
import {WebSocketLink} from 'apollo-link-ws';
import {getMainDefinition} from 'apollo-utilities';
import IApiAuthHolder from '../IApiAuthHolder';
import GraphqlApiError from './GraphqlApiError';

export default abstract class ApiBase<AuthInfo> {
  private readonly apolloClient: ApolloClient<NormalizedCacheObject>;

  constructor(
    baseUrl: string,
    wsBaseUrl: string,
    private configuration: ApiConfiguration,
    delegate: ApiDelegate<AuthInfo>,
    private tokenHolder: IApiAuthHolder<AuthInfo>,
  ) {
    this.apolloClient = new ApolloClient({
      link: ApiBase.createLink(baseUrl, wsBaseUrl, configuration, delegate, tokenHolder),
      cache: new InMemoryCache(),
      defaultOptions: {
        query: {
          fetchPolicy: 'no-cache',
        },
        mutate: {
          fetchPolicy: 'no-cache',
        },
        watchQuery: {
          fetchPolicy: 'no-cache',
        },
      },
    });
  }

  private static createLink<AuthInfo>(
    baseUrl: string,
    wsBaseUrl: string,
    configuration: ApiConfiguration,
    delegate: ApiDelegate<AuthInfo>,
    tokenHolder: IApiAuthHolder<AuthInfo>,
  ) {
    const authContext = () =>
      setContext((_, {headers}) => {
        return {
          headers: {
            ...headers,
            ...delegate.getHeaders(tokenHolder.getAuthInfo()),
          },
        };
      });

    const httpLink = authContext().concat(
      new HttpLink({
        uri: `${baseUrl}${configuration.path}`,
      }),
    );

    if (configuration.connectWebsocketTransport) {
      const wsLink = authContext().concat(
        new WebSocketLink({
          uri: `${wsBaseUrl}${configuration.path}`,
          options: {
            reconnect: true,
          },
          // todo: add headers
        }),
      );

      return split(
        ({query}) => {
          const definition = getMainDefinition(query);
          return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
          );
        },
        wsLink,
        httpLink,
      );
    }

    return httpLink;
  }

  protected async query<TVariables, TData>({
    query,
    variables,
  }: Query<TVariables, TData>): Promise<TData> {
    const {data} = await this.rawQuery(query, variables).catch((e) =>
      ApiBase.handleError(e),
    );
    return data as TData;
  }

  protected async mutation<TVariables, TData>({
    query,
    variables,
  }: Query<TVariables, TData>) {
    const {data} = await this.rawMutate(query, variables).catch((e) =>
      ApiBase.handleError(e),
    );
    return data as TData;
  }

  protected async subscribe<TVariables, TData>({
    query,
    variables,
  }: Query<TVariables, TData>) {
    if (!this.configuration.connectWebsocketTransport) {
      throw new Error(
        'Please, turn on "connectWebsocketTransport" parameter in configuration to use subscribe method',
      );
    }
    const subscription = await this.rawSubscribe(query, variables);
    return subscription.map(({data}) => data as TData);
  }

  public async rawQuery<TResult>(query: any, variables?: OperationVariables) {
    return this.apolloClient.query<TResult>({
      query,
      variables,
    });
  }

  public async rawMutate<TResult>(mutation: any, variables?: OperationVariables) {
    return this.apolloClient.mutate<TResult>({
      mutation,
      variables,
    });
  }

  public async rawSubscribe<TResult>(
    subscription: any,
    variables: OperationVariables | undefined,
  ) {
    return this.apolloClient.subscribe<TResult>({
      query: subscription,
      variables,
    });
  }

  private static handleError(error: ApolloError) {
    return Promise.reject(new GraphqlApiError(error.message, error));
  }
}
