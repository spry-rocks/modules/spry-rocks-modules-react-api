import IApiAuthHolder from './IApiAuthHolder';

export default interface IApiBase<AuthInfo> {
  getAuthHolder(): IApiAuthHolder<AuthInfo>;
}
