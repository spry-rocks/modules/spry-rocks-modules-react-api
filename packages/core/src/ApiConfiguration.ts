import GraphqlApiConfiguration from './graphql/ApiConfiguration';
import RestApiConfiguration from './rest/ApiConfiguration';

export default interface ApiConfiguration {
  host: string;
  port?: number;
  securePort?: number;
  globalPrefix: string | undefined;
  graphql?: GraphqlApiConfiguration;
  rest?: RestApiConfiguration;
  secure?: boolean;
  refreshTokensOnNotAuthorizedError?: boolean;
}

export function getApiConnectionOptions(configuration: ApiConfiguration) {
  const {host, globalPrefix} = configuration;
  const secure = configuration.secure || false;
  const insecurePort = configuration.port || 80;
  const securePort = configuration.securePort || 443;
  const port = secure ? securePort : insecurePort;

  const httpSchema = secure ? 'https' : 'http';
  let baseUrl = `${httpSchema}://${host}:${port}`;
  if (globalPrefix) baseUrl += globalPrefix;

  const wsSchema = secure ? 'wss' : 'ws';
  let wsBaseUrl = `${wsSchema}://${host}:${port}`;
  if (globalPrefix) wsBaseUrl += globalPrefix;

  return {
    baseUrl,
    wsBaseUrl,
  };
}
