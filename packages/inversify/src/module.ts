import {IRestNetworkFactory, RestNetworkFactory} from '@spryrocks/react-api';
import {ContainerModule, interfaces} from 'inversify';

const NetworkFactory = Symbol.for('Api.NetworkFactory');

type ServiceIdentifier<T> =
  | string
  | symbol
  | interfaces.Newable<T>
  | interfaces.Abstract<T>;

export const createModule = <IApi>(options: {
  setupModule?: (bind: interfaces.Bind) => void;
  networkFactory?: {constantValue?: IRestNetworkFactory};
  api: {
    apiIdentifier: ServiceIdentifier<IApi>;
    constantValue?: IApi;
    createApi: (context: interfaces.Context) => IApi;
  };
}) =>
  new ContainerModule((bind) => {
    if (options.setupModule) options.setupModule(bind);

    if (options.networkFactory?.constantValue) {
      bind(NetworkFactory).toConstantValue(options.networkFactory.constantValue);
    } else {
      bind(NetworkFactory).toConstantValue(new RestNetworkFactory());
    }

    if (options.api.constantValue) {
      bind(options.api.apiIdentifier).toConstantValue(options.api.constantValue);
    } else {
      bind(options.api.apiIdentifier)
        .toDynamicValue((context) => {
          if (!options.api?.createApi)
            throw new Error('Please provide options.api.createApi function');
          return options.api.createApi(context);
        })
        .inSingletonScope();
    }
  });
