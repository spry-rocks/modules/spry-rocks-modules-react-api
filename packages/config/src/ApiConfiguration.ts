import {ApiConfiguration} from '@spryrocks/react-api';
import {IConfigService} from '@spryrocks/config-react';

export const createConfiguration = (
  configService: IConfigService,
  override?: Partial<ApiConfiguration>,
): ApiConfiguration => ({
  host: configService.get('REACT_APP_API_HOST'),
  port: configService.getNumberOptional('REACT_APP_API_PORT'),
  securePort: configService.getNumberOptional('REACT_APP_API_SECURE_PORT'),
  globalPrefix: configService.getOptional('REACT_APP_API_GLOBAL_PREFIX'),
  rest: {
    path: configService.get('REACT_APP_API_REST_PATH'),
  },
  secure: configService.getBooleanOptional('REACT_APP_API_SECURE'),
  ...override,
});
